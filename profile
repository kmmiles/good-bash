################################################################################
# do initialiazation. set options and paths depending on whether this profile 
# was sourced from a script or directly on the shell.
################################################################################

if [ ! -z "${BASH_SOURCE[1]:-}" ]; then
    # sourced from script
    set -o errexit
    set -o pipefail
    set -o nounset

    __dir="$(cd "$(dirname "${BASH_SOURCE[1]}")" && pwd)"
    __file="${__dir}/$(basename "${BASH_SOURCE[1]}")"
    __root="$(cd "$(dirname "${__dir}")" && pwd)"
else
    # sourced from shell
    __dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
    __file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
    __root="${__dir}"
fi

if [[ "${DEBUG:-}" ]]; then
    echo "*** Debug mode enabled ***"
    set -o xtrace
else
    set +o xtrace
fi
